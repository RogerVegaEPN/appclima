//
//  ByCityViewController.swift
//  AppClimaGR2
//
//  Created by Roger on 6/11/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ByCityViewController: UIViewController {

    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getWeatherByCity(_ sender: Any) {
        print(cityTextField.text)
        getWeatherByLocation(cityTextField.text!)
    }
    
    private func getWeatherByLocation(_ city: String){
        /*
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=4dc66df3da5836ab5e1495ae8c823aaf"
        let url = URL(string:urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request){ (data, response, error) in
            
            if let _ = error{
                return
            }
            
            let weatherData = data as! Data
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let weather = weatherArray[0] as! NSDictionary
                //print(weatherDictionary["name"])
                let weatherDesc = weather["description"] ?? "error"
                print(weatherDesc)
                DispatchQueue.main.async{
                    self.weatherLabel.text = "\(weatherDesc)"
                }
                
            }catch{
                print ("Error")
            }
            */
            let service = Servicio()
            service.consultaPorCiudad(city: cityTextField.text!) { (weather) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
            }

        }
       // task.resume()
    }

}

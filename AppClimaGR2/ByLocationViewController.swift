
import UIKit
import CoreLocation

class ByLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager() //Para pedir permiso de la ubicacion
    var didGetWheater = false
  
  //MARK:- Outlets
  @IBOutlet weak var weatherLabel: UILabel!
  @IBOutlet weak var cityLabel: UILabel!
    
    override func viewDidLoad() {
 
    
    super.viewDidLoad()
    locationManager.requestWhenInUseAuthorization() //Solo cuando la app este activa
    //locationManager.requestAlwaysAuthorization() //Tiene su ubicacion incluso si esta minimizada la app
    
    if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
        locationManager.delegate = self //El delegado responda a esta clase
        locationManager.startUpdatingLocation() //Capturamos la ubicacion del telefono
    }
    
   
    
  }
    //MARK:- CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        print("La ubicacion es \(location)")
        if(!didGetWheater){
            getWeatherByLocation(
                lat: (location?.latitude)!,
                lon: (location?.longitude)!
            )
            didGetWheater = true;
        }
    }
    //MARK:- Actions
    private func getWeatherByLocation(lat: Double, lon:Double){
        /*
        print("Entra getweather")
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=4dc66df3da5836ab5e1495ae8c823aaf"
        let url = URL(string:urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request){ (data, response, error) in
            
            if let _ = error{
                return
            }
            
            let weatherData = data as! Data
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let weather = weatherArray[0] as! NSDictionary
                let cityName = weatherDictionary["name"] as! String
                print(weatherArray)
                let weatherDesc = weather["description"] ?? "error"
                
                DispatchQueue.main.async{
                    self.weatherLabel.text = "\(weatherDesc)"
                    self.cityLabel.text = "\(cityName)"
                }
                
            }catch{
                print ("Error")
            }
            
            //print(type(of: data))
            //print(response)
            //print(error)
 */
            let service = Servicio()
            service.consultaPorUbicacion(lat: lat, lon: lon) { (weather) in
                DispatchQueue.main.async {
                    var result = weather.components(separatedBy: "&")
                    self.weatherLabel.text = result[0]
                    self.cityLabel.text = result[1]
                }
            }
        //task.resume()
    }
    
    
    @IBOutlet weak var ExitButton: UIButton!
    
    
    @IBAction func salirButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil) //pasa a la ventana que estaba atras
    }
}

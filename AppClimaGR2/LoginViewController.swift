

import UIKit

class LoginViewController: UIViewController {
  
  //MARK:- Outlets
  
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  
  //MARK:- ViewController Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
    
    
  
  //MARK:- Actions
  
  @IBAction func entrarButtonPressed(_ sender: Any) {
    let username = usernameTextField.text!
    let password = passwordTextField.text!
    
    switch(username, password){
    case ("roger","roger"):
        performSegue(withIdentifier: "citySegue", sender: self)
        print("Login correcto")
    default:
        showAlertMessage(message: "Login incorrecto")
    }
    
  }
    
    private func showAlertMessage(message : String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .actionSheet)
        
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.usernameTextField.text = ""
            self.passwordTextField.text = ""
        }
        
        
        let acceptAction2 = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            self.usernameTextField.text = ""
            self.passwordTextField.text = ""
        }
        
        alertController.addAction(acceptAction)
        alertController.addAction(acceptAction2)
        
        present(alertController, animated: true, completion: nil)
    }
  
  @IBAction func invitadoButtonPressed(_ sender: Any) {
  }
  
}

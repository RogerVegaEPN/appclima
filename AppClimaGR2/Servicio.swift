//
//  Servicio.swift
//  AppClimaGR2
//
//  Created by Roger on 10/11/17.
//  Copyright © 2017 SG. All rights reserved.
//

import Foundation


class Servicio {
    
    func consultaPorCiudad (city:String, completion:@escaping (String) -> ()) {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=4dc66df3da5836ab5e1495ae8c823aaf"
        
        consulta(urlStr: urlStr) { (weather, ciudad) in
            completion(weather)
        }
    }
    
    func consultaPorUbicacion (lat:Double, lon:Double, completion: @escaping (String) -> ()){
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=4dc66df3da5836ab5e1495ae8c823aaf"
        
        consulta(urlStr: urlStr, flag: true) { (weather, ciudad) in
            completion(weather)
        }
    }
    
    
    func consulta(urlStr:String, flag: Bool = false, completion:@escaping (String, String)-> ()){
        
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let _ = error {
                return
            }
            
            do {
                
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                guard let weatherKey = weatherDictionary["weather"] as? NSArray else {
                    completion("Ciudad no valida", "ciudad")
                    return
                }
                
                let weather = weatherKey[0] as! NSDictionary
                var respuesta:String = weather["description"] as! String
                
                if(flag){
                    let cityName = weatherDictionary["name"] as! String
                    respuesta +=  "&" + cityName
                }
                
                //completion("\(weather["description"]", "ciudad")
                completion(respuesta, "ciudad")
                
                
            } catch {
                print("Error")
            }
        }
        task.resume()
    }
}
